package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.service.IAuthService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.api.service.IUserService;
import ru.t1.sochilenkov.tm.exception.field.LoginEmptyException;
import ru.t1.sochilenkov.tm.exception.field.PasswordEmptyException;
import ru.t1.sochilenkov.tm.exception.system.AccessDeniedException;
import ru.t1.sochilenkov.tm.model.User;
import ru.t1.sochilenkov.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    final IPropertyService propertyService;

    public AuthService(
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public User check(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final boolean locked = user.getLocked();
        if (locked) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || !hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return user;
    }

}
