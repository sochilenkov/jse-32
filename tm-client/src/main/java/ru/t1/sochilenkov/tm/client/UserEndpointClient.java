package ru.t1.sochilenkov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.endpoint.IUserEndpointClient;
import ru.t1.sochilenkov.tm.dto.request.*;
import ru.t1.sochilenkov.tm.dto.response.*;

@NoArgsConstructor
public final class UserEndpointClient extends AbstractEndpointClient implements IUserEndpointClient {

    public UserEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @Override
    @NotNull
    public UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @Override
    @NotNull
    public UserLockResponse lockUser(@NotNull final UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @Override
    @NotNull
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @Override
    @NotNull
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @Override
    @NotNull
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @Override
    @NotNull
    public UserUpdateProfileResponse updateUserProfile(@NotNull final UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

}
