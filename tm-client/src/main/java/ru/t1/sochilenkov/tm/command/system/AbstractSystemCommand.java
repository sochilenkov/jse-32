package ru.t1.sochilenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.endpoint.ISystemEndpointClient;
import ru.t1.sochilenkov.tm.api.service.ICommandService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected ISystemEndpointClient getSystemEndpoint() {
        return serviceLocator.getSystemEndpointClient();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

}
