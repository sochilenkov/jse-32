package ru.t1.sochilenkov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.endpoint.IDomainEndpointClient;
import ru.t1.sochilenkov.tm.dto.request.*;
import ru.t1.sochilenkov.tm.dto.response.*;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpointClient {

    public DomainEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @Override
    @NotNull
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @Override
    @NotNull
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @Override
    @NotNull
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @Override
    @NotNull
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @Override
    @NotNull
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @Override
    @NotNull
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @Override
    @NotNull
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull final DataJsonLoadFasterXmlRequest request) {
        return call(request, DataJsonLoadFasterXmlResponse.class);
    }

    @Override
    @NotNull
    public DataJsonLoadJaxBResponse loadDataJsonJaxB(@NotNull final DataJsonLoadJaxBRequest request) {
        return call(request, DataJsonLoadJaxBResponse.class);
    }

    @Override
    @NotNull
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull final DataJsonSaveFasterXmlRequest request) {
        return call(request, DataJsonSaveFasterXmlResponse.class);
    }

    @Override
    @NotNull
    public DataJsonSaveJaxBResponse saveDataJsonJaxB(@NotNull final DataJsonSaveJaxBRequest request) {
        return call(request, DataJsonSaveJaxBResponse.class);
    }

    @Override
    @NotNull
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull final DataXmlLoadFasterXmlRequest request) {
        return call(request, DataXmlLoadFasterXmlResponse.class);
    }

    @Override
    @NotNull
    public DataXmlLoadJaxBResponse loadDataXmlJaxB(@NotNull final DataXmlLoadJaxBRequest request) {
        return call(request, DataXmlLoadJaxBResponse.class);
    }

    @Override
    @NotNull
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull final DataXmlSaveFasterXmlRequest request) {
        return call(request, DataXmlSaveFasterXmlResponse.class);
    }

    @Override
    @NotNull
    public DataXmlSaveJaxBResponse saveDataXmlJaxB(@NotNull final DataXmlSaveJaxBRequest request) {
        return call(request, DataXmlSaveJaxBResponse.class);
    }

    @Override
    @NotNull
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull final DataYamlLoadFasterXmlRequest request) {
        return call(request, DataYamlLoadFasterXmlResponse.class);
    }

    @Override
    @NotNull
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull final DataYamlSaveFasterXmlRequest request) {
        return call(request, DataYamlSaveFasterXmlResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("admin", "admin")));
            @NotNull final DomainEndpointClient domainClient = new DomainEndpointClient(authEndpointClient);
            domainClient.saveDataBase64(new DataBase64SaveRequest());
        }
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("user 1", "1")));
            @NotNull final DomainEndpointClient domainClient = new DomainEndpointClient(authEndpointClient);
            domainClient.saveDataBase64(new DataBase64SaveRequest());
        }
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

}
