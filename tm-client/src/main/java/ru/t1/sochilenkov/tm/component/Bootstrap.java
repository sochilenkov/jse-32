package ru.t1.sochilenkov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.sochilenkov.tm.api.endpoint.*;
import ru.t1.sochilenkov.tm.api.repository.*;
import ru.t1.sochilenkov.tm.api.service.*;
import ru.t1.sochilenkov.tm.client.*;
import ru.t1.sochilenkov.tm.command.AbstractCommand;
import ru.t1.sochilenkov.tm.command.server.ConnectCommand;
import ru.t1.sochilenkov.tm.command.server.DisconnectCommand;
import ru.t1.sochilenkov.tm.repository.*;
import ru.t1.sochilenkov.tm.service.*;
import ru.t1.sochilenkov.tm.util.SystemUtil;
import ru.t1.sochilenkov.tm.util.TerminalUtil;
import ru.t1.sochilenkov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.sochilenkov.tm.exception.system.CommandNotSupportedException;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final String PACKAGE_COMMANDS = "ru.t1.sochilenkov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @NotNull
    @Getter
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    @Getter
    private final IEndpointClient connectionEndpointClient = new ConnectionEndpointClient();

    @NotNull
    @Getter
    private final ISystemEndpointClient systemEndpointClient = new SystemEndpointClient();

    @NotNull
    @Getter
    final ITaskEndpointClient taskEndpointClient = new TaskEndpointClient();

    @NotNull
    @Getter
    final IProjectEndpointClient projectEndpointClient = new ProjectEndpointClient();

    @NotNull
    @Getter
    final IUserEndpointClient userEndpointClient = new UserEndpointClient();

    @NotNull
    @Getter
    final IDomainEndpointClient domainEndpointClient = new DomainEndpointClient();

    @NotNull
    @Getter
    final IAuthEndpointClient authEndpointClient = new AuthEndpointClient();

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void connect() {
        processCommand(ConnectCommand.NAME);
    }

    private void disconnect() {
        processCommand(DisconnectCommand.NAME);
    }

    private void processArguments(@Nullable String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private static void exit() {
        System.exit(0);
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) {
        @Nullable AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null && argument != null) throw new ArgumentNotSupportedException(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
        disconnect();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
        connect();
    }

    public void run(@Nullable String[] args) {
        processArguments(args);
        prepareStartup();
        initCommands();
    }

}
