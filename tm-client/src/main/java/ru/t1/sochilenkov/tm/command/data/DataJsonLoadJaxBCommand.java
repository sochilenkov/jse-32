package ru.t1.sochilenkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.dto.request.DataJsonLoadJaxBRequest;

public class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load data from json file.";

    @NotNull
    public static final String NAME = "data-load-json-jaxb";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final DataJsonLoadJaxBRequest request = new DataJsonLoadJaxBRequest();
        getDomainEndpoint().loadDataJsonJaxB(request);
    }

}
