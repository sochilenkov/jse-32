package ru.t1.sochilenkov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.endpoint.ISystemEndpointClient;
import ru.t1.sochilenkov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.sochilenkov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.sochilenkov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.sochilenkov.tm.dto.response.ApplicationVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    public SystemEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ApplicationAboutResponse getAbout(@NotNull final ApplicationAboutRequest request) {
        return call(request, ApplicationAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ApplicationVersionResponse getVersion(@NotNull final ApplicationVersionRequest request) {
        return call(request, ApplicationVersionResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        final ApplicationAboutResponse applicationAboutResponse = client.getAbout(new ApplicationAboutRequest());
        System.out.println(applicationAboutResponse.getEmail());
        System.out.println(applicationAboutResponse.getName());

        final ApplicationVersionResponse applicationVersionResponse = client.getVersion(new ApplicationVersionRequest());
        System.out.println(applicationVersionResponse.getVersion());

        client.disconnect();
    }

}
