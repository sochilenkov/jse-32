package ru.t1.sochilenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.sochilenkov.tm.api.endpoint.IUserEndpointClient;
import ru.t1.sochilenkov.tm.command.AbstractCommand;
import ru.t1.sochilenkov.tm.exception.entity.UserNotFoundException;
import ru.t1.sochilenkov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IUserEndpointClient getUserEndpoint() {
        return serviceLocator.getUserEndpointClient();
    }

    @NotNull
    public IAuthEndpointClient getAuthEndpoint() {
        return serviceLocator.getAuthEndpointClient();
    }

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
