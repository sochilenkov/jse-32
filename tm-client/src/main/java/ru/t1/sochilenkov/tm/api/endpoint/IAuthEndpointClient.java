package ru.t1.sochilenkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.dto.request.UserLoginRequest;
import ru.t1.sochilenkov.tm.dto.request.UserLogoutRequest;
import ru.t1.sochilenkov.tm.dto.request.UserProfileRequest;
import ru.t1.sochilenkov.tm.dto.response.UserLoginResponse;
import ru.t1.sochilenkov.tm.dto.response.UserLogoutResponse;
import ru.t1.sochilenkov.tm.dto.response.UserProfileResponse;

public interface IAuthEndpointClient extends IEndpointClient {

    @NotNull
    UserLoginResponse login(@NotNull final UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull final UserLogoutRequest request);

    @NotNull
    UserProfileResponse profile(@NotNull UserProfileRequest request);

}
