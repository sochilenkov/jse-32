package ru.t1.sochilenkov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationAboutResponse extends AbstractResponse {

    public String email;

    private String name;

}
