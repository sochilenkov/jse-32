package ru.t1.sochilenkov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.model.Project;

import java.util.List;

@NoArgsConstructor
public final class ProjectListResponse extends AbstractProjectResponse {

    @Getter
    @Nullable
    List<Project> projects;

    public ProjectListResponse(@Nullable final List<Project> projects) {
        this.projects = projects;
    }

}
